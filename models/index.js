'use strict';

const mongoose = require('mongoose');
const config = require('../configuration');
const logger = require("../logger");
// const db = require('../database');

let databaseFullURL = "mongodb://" + config.database.user + ":" + config.database.pwd + "@" + config.database.host + ":" + config.database.port + "/" + config.database.database;
let promise = mongoose.connect(databaseFullURL);

let Schema = mongoose.Schema;

/** CDR schema */
let CDRSchema = new Schema({
    iccid: String,
    msisdn: String,
    createdAt: Date,
    bytes: Number,
    mcc: String,
    mnc: String,
    promotionName: String,
    call: {
        mcc: String,
        mnc: String,
        startTime: String,
        endTime: String,
        bytes: Number,
        bytesInc: Number,
        cost: Number,
        id: String,
        ip: String
    }
});
var CDRModel = mongoose.model("CDR", CDRSchema, 'cdr');

/** Exports */
module.exports = {
    CDR: CDRModel
};