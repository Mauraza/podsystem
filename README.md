# Exercise

The exercise consist in create an script that should collect all the CDRs of May 1st, 2018 from both sources and collate it\*, use the "call.endTime" field as a guide to find it. With this information you have to generate a CSV\*\* file with the CDRs you miss in the Source 1.

\* You can difference CDRs by two different ways, by "call.id" or by "iccid" + "call.startTime" + "call.endTime"

\*\* CSV header: "\_id","call.id","iccid","msisdn","customer","call.startTime","call.endTime","call.bytesInc","promotion.active.name"

## Run the solution

For script execution:

- download the repository
- run "yarn install"
- Modify the parameters if necessary
- run "yarn start"
- Result in "result.csv"

## More information:

Parameters (found under configuration/configuration.yaml):

- database connection
- url of the api
- the variable for the date range increment (it's minutes)

Packages used:

- "bluebird": promise librarie
- "csv-array": CSV-parser for nodeJS
- "fs": file management
- "js-yaml": loader of yaml file
- "jslint": the JavaScript Code Quality Tool,
- "json2csv": for parse json to csv
- "moment": for date in javascript
- "mongoose": ORM of mongoDB
- "path": a module provides utilities for working with file and directory paths
- "request": module to implest way possible to make http calls
- "winston": packege for the logs

Small explanation of the solution :
I have chosen the solution of making requests with an increasing time range of the startTime. I compare the elements of the two sources, considering that it is in the database where certain elements may not exist, i.e. I look at which of the api elements are not in the database.

Time spent in the resolution of the exercise approximately 14 hours
