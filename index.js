"use strict";

const request = require("request");
const Promise = require("bluebird");
const moment = require("moment");
const fs = require("fs");
const csv = require("csv-array");
const config = require("./configuration");
const logger = require("./logger");
const db = require("./database");
const Json2csvParser = require("json2csv").Parser;

var startTime = moment("2018-05-01 00:00:00");
var endTime = moment("2018-05-02 00:00:00");

findDiffCDR(startTime);

// async function
async function findDiffCDR(startTime) {
  // Start Time
  var total = 0;
  var notDifferent = 0;
  var rangeDate = moment("2018-05-01 00:00:00");
  rangeDate.add(config.data.time, "h");
  var optionsAPI =
    "?customer=all&sort=_id&output=csv&endTime=" +
    startTime.format("YYYY-MM-DD HH:mm:ss") +
    "," +
    endTime.format("YYYY-MM-DD HH:mm:ss") +
    "," +
    "&startTime=";
  // date for the tests
  // var finalizedTime = moment('2018-05-01 00:30:00');
  var optionsDB = moment(startTime);
  var missing = [];
  while (rangeDate.isSameOrBefore(endTime)) {
    let responseAPICSV = await getCdrApiCsv(
      optionsAPI + startTime.format("YYYY-MM-DD HH:mm:ss") + "," + rangeDate.format("YYYY-MM-DD HH:mm:ss") + ","
    );
    let responseAPI = await getArray(responseAPICSV);
    logger.info("Data of the api");
    logger.info(responseAPI.length);
    let responseDB = await getCDRDB(
      startTime.format("YYYY-MM-DD HH:mm:ss"),
      rangeDate.format("YYYY-MM-DD HH:mm:ss"),
      optionsDB.format("YYYY-MM-DD HH:mm:ss"),
      endTime.format("YYYY-MM-DD HH:mm:ss")
    );
    logger.info("Data of the db");
    logger.info(responseDB.length);
    let miss = await compare(responseAPI, responseDB, total, notDifferent);
    logger.info("Not equal");
    logger.info(miss.length);
    missing.push(miss);
    rangeDate = rangeDate.add(config.data.time, "h");
    startTime.add(config.data.time, "h");
  }
  var dataToCSV = await getCdr(missing);
  var result = await toCSV(dataToCSV);
  // logger.info(result);
  generateCSVFile(result);
  logger.info(missing.length);
}

// Request to the api
function getCdrApiCsv(options) {
  return new Promise(function(resolve, reject) {
    var myUrl = config.api.url + options;
    logger.info("Options API: " + myUrl);
    request.get(myUrl, function(err, res, body) {
      if (err) {
        logger.debug("error: " + error);
        // return 400;
        return reject(error);
      } else {
        logger.debug("Response of the api");
        // logger.debug(body);
        //return resolve(body);
        var nameFile = "test.csv";
        fs.writeFile(nameFile, body, function(err) {
          if (err) {
            return reject(err);
          }
          return resolve(nameFile);
        });
      }
    });
  });
}

// the output to array
function getArray(route) {
  return new Promise(function(resolve, reject) {
    csv.parseCSV(route, function(data) {
      logger.debug("Create array");
      return resolve(data);
    });
  });
}
// Get data of the database
function getCDRDB(startTime1, startTime2, endTime1, endTime2) {
  return new Promise(function(resolve, reject) {
    db.getCDR(startTime1, startTime2, endTime1, endTime2).then(data => {
      logger.info("Get data of the database");
      return resolve(data);
    });
  });
}

// Compare the data of the api with the data of the DB
function compare(responseAPI, responseDB, total, notDifferent) {
  logger.info("compare");
  total += responseAPI.length;
  var notFound = [];
  responseAPI.forEach(elementAPI => {
    if (equalsCDR(elementAPI, responseDB)) {
      notDifferent += 1;
    } else {
      notFound.push(elementAPI);
    }
  });
  return notFound;
}

// Check if tow CDR are equals
function equalsCDR(cdrCSV, responseDB) {
  var find = false;
  for (let i = 0; i < responseDB.length && !find; i++) {
    if (
      cdrCSV['"iccid"'].replace("'", "") === responseDB[i].iccid &&
      cdrCSV['"startTime(UTC)"'] === responseDB[i].call.startTime &&
      cdrCSV['"endTime(UTC)"'] === responseDB[i].call.endTime
    ) {
      find = true;
    }
  }
  return find;
}

// get cdrs form the api
function getCdr(missing) {
  return new Promise(function(resolve, reject) {
    var objectsCdr = [];
    var promises = [];
    missing.forEach(elements => {
      elements.forEach(value => {
        promises.push(
          apiRequest(
            "?customer=all&startTime=" + value['"startTime(UTC)"'] + "&endTime=" + value['"endTime(UTC)"'],
            value['"iccid"'].replace("'", "")
          )
        );
      });
    });
    Promise.all(promises).then(cdr => {
      return resolve(cdr);
    });
  });
}

// request to the api
function apiRequest(options, iccid) {
  return new Promise(function(resolve, reject) {
    var myUrl = config.api.url + options;
    // logger.info('Options API: ' + myUrl);
    request.get(myUrl, function(err, res, body) {
      if (err) {
        logger.debug("error: " + error);
        // return 400;
        return reject(error);
      } else {
        var resultAPI = JSON.parse(body);
        if (resultAPI.length > 1) {
          resultAPI.forEach(response => {
            if (response.iccid === iccid) {
              return resolve(response);
            }
          });
        } else {
          return resolve(resultAPI[0]);
        }
      }
    });
  });
}

// date to csv
function toCSV(data) {
  return new Promise(function(resolve, reject) {
    // var fields =["\"iccid\",\"msisdn\",\"startTime(UTC)\",\"endTime(UTC)\",\"bytes\",\"mcc\",\"mnc\",\"promotionName\""];
    var fields = [
      "_id",
      "call.id",
      "iccid",
      "msisdn",
      "customer",
      "call.startTime",
      "call.endTime",
      "call.bytesInc",
      "promotion.active.name"
    ];
    var parser = new Json2csvParser({
      fields
    });
    try {
      var csv = parser.parse(data);
      return resolve(csv);
    } catch (err) {
      logger.error(err);
      return reject(err);
    }
  });
}

// generate a csv file
function generateCSVFile(result) {
  logger.info("Generate the result.csv");
  return new Promise(function(resolve, reject) {
    fs.writeFile("result.csv", result, function(err) {
      if (err) {
        return reject(err);
      }
      return resolve("result.csv");
    });
  });
}
