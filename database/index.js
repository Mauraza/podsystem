"use strict";

const mongoose = require("mongoose");
const Promise = require("bluebird");
const models = require("../models");
const config = require("../configuration");
const logger = require("../logger");

let databaseFullURL =
    "mongodb://" +
    config.database.user +
    ":" +
    config.database.pwd +
    "@" +
    config.database.host +
    ":" +
    config.database.port +
    "/" +
    config.database.database;

mongoose.Promise = global.Promise;

var promise = mongoose.connect(databaseFullURL);

module.exports = {
    models: models,
    getCDR: _getCDR
};

function _getCDR(startTime1, startTime2, endTime1, endTime2) {
    return new Promise(function (resolve, reject) {
        models.CDR.find({
                'call.endTime': {
                    $gte: endTime1,
                    $lte: endTime2
                },
                'call.startTime': {
                    $gte: startTime1,
                    $lte: startTime2
                },
            },
            function (err, data) {
                if (err) {
                    logger.error(err); //internal server error
                    return reject(err);
                } else {
                    return resolve(data); //get groups
                }
            }
        );
    });
}