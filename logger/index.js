'use strict';

/**
 * Module dependencies.
 * */

const winston = require('winston');
const config = require('../configuration');

/**
 * Configure here your custom levels.
 * */
const customLevels = {
  levels: {
    error: 7,
    warning: 8,
    restalk: 10,
    info: 11,
    test: 12,
    debug: 13
  },
  colors: {
    error: 'red',
    warning: 'yellow',
    restalk: 'white',
    info: 'white',
    test: 'magenta',
    debug: 'black'
  }
};

winston.emitErrs = true;
const logger = new winston.Logger({
  levels: customLevels.levels,
  colors: customLevels.colors,
  transports: [
    new winston.transports.File({
      createTree: false,
      level: config.log.level,
      filename: config.log.file,
      handleExceptions: true,
      json: false,
      tailable: true,
      maxsize: config.log.maxSize,
      maxFiles: config.log.maxFiles
    }),
    new winston.transports.Console({
      level: config.loglevel,
      handleExceptions: true,
      json: false,
      colorize: true,
      timestamp: true
    })
  ],
  exitOnError: false
});

/*
 * Export functions and Objects
 */
module.exports = logger;